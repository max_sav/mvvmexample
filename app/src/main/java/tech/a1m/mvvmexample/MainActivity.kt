package tech.a1m.mvvmexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance
import tech.a1m.mvvmexample.modules.settings.AuthInfo
import tech.a1m.mvvmexample.screens.auth.login.LoginFragment
import tech.a1m.mvvmexample.screens.main.MainFragment

class MainActivity : AppCompatActivity(), KodeinAware {

    override val kodein by closestKodein()

    private val authInfo by instance<AuthInfo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(
                    R.id.content,
                    if (authInfo.isAuth) MainFragment() else LoginFragment()
                )
            }
        }
    }
}
