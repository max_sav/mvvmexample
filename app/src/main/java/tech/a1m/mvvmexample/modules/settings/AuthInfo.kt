package tech.a1m.mvvmexample.modules.settings

import android.content.SharedPreferences
import androidx.core.content.edit
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import tech.a1m.mvvmexample.Mockable

val authModule = Kodein.Module(name = "authModule", allowSilentOverride = true) {
    bind<AuthInfo>() with singleton { AuthInfo(instance()) }
}

@Mockable
class AuthInfo(private val sharedPreferences: SharedPreferences) {

    companion object {
        const val FIELD_IS_AUTH = "is_auth"
    }

    var isAuth: Boolean
        set(value) {
            sharedPreferences.edit {
                putBoolean(FIELD_IS_AUTH, value)
            }
        }
        get() = sharedPreferences.getBoolean(FIELD_IS_AUTH, false)
}