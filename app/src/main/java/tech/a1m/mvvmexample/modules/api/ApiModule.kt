package tech.a1m.mvvmexample.modules.api

import androidx.annotation.WorkerThread
import io.reactivex.Single
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import tech.a1m.mvvmexample.Mockable
import java.io.IOException
import kotlin.random.Random

private const val LOGIN = "test"
private const val PASSWORD = "123456"

val apiModule = Kodein.Module("apiModule") {
    bind<Api>() with singleton { Api() }
}

// fake api
@Mockable
class Api {

    @WorkerThread
    @Throws(IOException::class)
    private fun internalAuth(login: String, password: String): Boolean {
        val rand = Random(System.currentTimeMillis()).nextBoolean()
        if (rand) {
            throw IOException()
        }
        return login == LOGIN && password == PASSWORD
    }

    fun auth(login: String, password: String) =
        Single.fromCallable { internalAuth(login, password) }

    @WorkerThread
    @Throws(IOException::class)
    private fun internalRestorePassword(login: String): String? {
        val rand = Random(System.currentTimeMillis()).nextBoolean()
        if (rand) {
            throw IOException()
        }
        return if (login == LOGIN) PASSWORD else null
    }

    fun restorePassword(login: String) =
        Single.fromCallable { internalRestorePassword(login) }
}