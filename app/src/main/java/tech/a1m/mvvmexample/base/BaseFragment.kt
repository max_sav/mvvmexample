package tech.a1m.mvvmexample.base

import androidx.annotation.VisibleForTesting
import androidx.fragment.app.Fragment
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein

abstract class BaseFragment : Fragment(), KodeinAware {

    private val parentKodein by closestKodein()

    protected open val kodeinBuilder: (Kodein.MainBuilder.() -> Unit)? = null

    @VisibleForTesting
    open var testKodeinModule: Kodein.Module? = null

    final override val kodein = Kodein.lazy {
        extend(parentKodein)
        kodeinBuilder?.invoke(this)
        testKodeinModule?.let { importOnce(module = it, allowOverride = true) }
    }
}