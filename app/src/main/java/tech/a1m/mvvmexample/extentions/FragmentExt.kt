@file:Suppress("NOTHING_TO_INLINE")

package tech.a1m.mvvmexample.extentions

import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

fun <T : ViewDataBinding> Fragment.bind() = requireView().bind<T>().apply {
    setLifecycleOwner(viewLifecycleOwner)
}

@Suppress("UNCHECKED_CAST")
inline fun <T> Fragment.arg(key: String) = lazy(mode = LazyThreadSafetyMode.NONE) {
    arguments?.get(key) as T
}