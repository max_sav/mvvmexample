@file:Suppress("NOTHING_TO_INLINE")

package tech.a1m.mvvmexample.extentions

import android.app.Application
import android.content.Context
import androidx.fragment.app.Fragment
import tech.a1m.mvvmexample.App

inline fun Context.getApplication() = applicationContext as Application
inline fun Context.getApp() = getApplication() as App

inline fun Fragment.requireApplication() = requireContext().getApplication()
inline fun Fragment.requireApp() = requireContext().getApp()
inline fun Fragment.getApplication() = context?.getApplication()
inline fun Fragment.getApp() = context?.getApp()