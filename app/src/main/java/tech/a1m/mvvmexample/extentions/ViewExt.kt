@file:Suppress("NOTHING_TO_INLINE")

package tech.a1m.mvvmexample.extentions

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

inline fun <T : ViewDataBinding> View.bind() = DataBindingUtil.bind<T>(this)!!