package tech.a1m.mvvmexample

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import tech.a1m.mvvmexample.modules.api.apiModule
import tech.a1m.mvvmexample.modules.settings.authModule

class App : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        importOnce(androidXModule(this@App))
        importOnce(authModule)
        importOnce(apiModule)
    }

    override fun onCreate() {
        super.onCreate()
    }
}