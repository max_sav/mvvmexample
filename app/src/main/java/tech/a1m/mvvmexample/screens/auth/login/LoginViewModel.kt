package tech.a1m.mvvmexample.screens.auth.login

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import tech.a1m.mvvmexample.Mockable
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.modules.api.Api
import tech.a1m.mvvmexample.modules.settings.AuthInfo
import tech.a1m.mvvmexample.utills.Event
import tech.a1m.mvvmexample.utills.NetworkState
import tech.a1m.mvvmexample.utills.Status
import java.util.concurrent.TimeUnit

@Mockable
class LoginViewModel(
    private val _login: String,
    private val api: Api,
    private val authInfo: AuthInfo,
    application: Application
) : AndroidViewModel(application) {
    private val disposables = CompositeDisposable()

    val login = MutableLiveData<String>().apply { value = _login }
    val password = MutableLiveData<String>().apply { value = "" }

    private val internalNetworkState = MutableLiveData<NetworkState>().apply {
        value = NetworkState.LOADED
    }
    val networkState: LiveData<NetworkState> get() = internalNetworkState

    val networkErrorEvent: LiveData<Event<String?>> = internalNetworkState.map {
        Event(
            if (it.status == Status.FAILED) {
                it.msg
            } else {
                null
            }
        )
    }

    private val internalPasswordError = MutableLiveData<String?>()
    val passwordError: LiveData<String?> get() = internalPasswordError

    private val internalSuccessLoginEvent = MutableLiveData<Event<Unit>>()
    val successLoginEvent: LiveData<Event<Unit>> get() = internalSuccessLoginEvent

    fun auth(login: String, password: String) {
        Single.fromCallable { login to password }
            .subscribeOn(Schedulers.io())
            .delay(5, TimeUnit.SECONDS)
            .flatMap { api.auth(it.first, it.second) }
            .doOnSubscribe { internalNetworkState.postValue(NetworkState.LOADING) }
            .observeOn(Schedulers.computation())
            .subscribe(
                {
                    internalNetworkState.postValue(NetworkState.LOADED)
                    if (it) {
                        internalSuccessLoginEvent.postValue(Event(Unit))
                        authInfo.isAuth = true
                    } else {
                        internalPasswordError.postValue(
                            getApplication<Application>().getString(R.string.bad_password)
                        )
                    }
                },
                {
                    internalNetworkState.postValue(
                        NetworkState.error(
                            getApplication<Application>().getString(R.string.network_error)
                        )
                    )
                }
            )
            .addTo(disposables)
    }

    fun auth() {
        val login = this.login.value
        val password = this.password.value
        if (login != null && password != null) {
            auth(login, password)
        }
    }

    fun clearErrors() {
        internalPasswordError.postValue(null)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }
}

@Mockable
class LoginViewModelFactory(
    private val login: String,
    private val api: Api,
    private val authInfo: AuthInfo,
    private val application: Application
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = LoginViewModel(
        login,
        api,
        authInfo,
        application
    ) as T
}