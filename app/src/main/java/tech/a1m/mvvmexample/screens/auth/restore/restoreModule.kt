package tech.a1m.mvvmexample.screens.auth.restore

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.factory
import org.kodein.di.generic.instance

val restoreModule = Kodein.Module(name = "restoreModule") {
    bind<RestoreViewModelFactory>() with factory { login: String ->
        RestoreViewModelFactory(login, instance(), instance())
    }
}