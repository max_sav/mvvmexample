package tech.a1m.mvvmexample.screens.auth.restore


import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ContentView
import androidx.core.os.bundleOf
import androidx.fragment.app.transaction
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import org.jetbrains.anko.design.snackbar
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.base.BaseFragment
import tech.a1m.mvvmexample.databinding.FragmentRestorePasswordBinding
import tech.a1m.mvvmexample.extentions.arg
import tech.a1m.mvvmexample.extentions.bind
import tech.a1m.mvvmexample.screens.main.MainFragment
import tech.a1m.mvvmexample.utills.Status

private const val ARG_LOGIN = "LOGIN"

fun newRestoreFragment(login: String): RestoreFragment {
    return RestoreFragment().apply {
        arguments = newRestoreFragmentBundle(login)
    }
}

fun newRestoreFragmentBundle(login: String) = bundleOf(
    ARG_LOGIN to login
)

@ContentView(R.layout.fragment_restore_password)
class RestoreFragment : BaseFragment() {

    override val kodeinBuilder: (Kodein.MainBuilder.() -> Unit)? = {
        importOnce(restoreModule)
    }

    val login by arg<String>(ARG_LOGIN)
    private val restoreViewModelFactory: RestoreViewModelFactory by instance { login }
    private val restoreViewModel by viewModels<RestoreViewModel> { restoreViewModelFactory }
    private lateinit var binding: FragmentRestorePasswordBinding

    @Suppress("DEPRECATION")
    private val progressDialog by lazy {
        ProgressDialog(requireContext()).apply {
            this.isIndeterminate = true
            this.setCanceledOnTouchOutside(false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_restore_password, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = bind()
        binding.viewModel = restoreViewModel

        restoreViewModel.networkErrorEvent.observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let {
                view.snackbar(it, getString(R.string.retry)) {
                    restoreViewModel.restorePassword()
                }
            }
        })

        val onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) restoreViewModel.clearErrors()
        }
        binding.loginEditText.onFocusChangeListener = onFocusChangeListener

        restoreViewModel.successLoginEvent.observe(this, Observer {
            requireFragmentManager().transaction {
                replace(R.id.content, MainFragment())
            }
        })

        restoreViewModel.networkState.observe(this, Observer {
            if (it.status == Status.RUNNING) {
                progressDialog.show()
            } else {
                progressDialog.dismiss()
            }
        })
    }

    override fun onDestroyView() {
        binding.unbind()
        super.onDestroyView()
    }
}
