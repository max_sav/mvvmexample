package tech.a1m.mvvmexample.screens.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import org.jetbrains.anko.startActivity
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance
import tech.a1m.mvvmexample.MainActivity
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.databinding.FragmentMainBinding
import tech.a1m.mvvmexample.modules.settings.AuthInfo

class MainFragment : Fragment(), KodeinAware {
    override val kodein by closestKodein()

    private val authInfo by instance<AuthInfo>()

    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)

        binding.logoutButton.setOnClickListener {
            authInfo.isAuth = false
            requireContext().startActivity<MainActivity>()
            requireActivity().finish()
        }
    }
}