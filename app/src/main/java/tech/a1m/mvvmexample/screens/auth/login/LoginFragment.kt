package tech.a1m.mvvmexample.screens.auth.login


import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ContentView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import org.jetbrains.anko.design.snackbar
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.base.BaseFragment
import tech.a1m.mvvmexample.databinding.FragmentLoginBinding
import tech.a1m.mvvmexample.extentions.bind
import tech.a1m.mvvmexample.screens.auth.restore.newRestoreFragment
import tech.a1m.mvvmexample.screens.main.MainFragment
import tech.a1m.mvvmexample.utills.Status

@ContentView(R.layout.fragment_login)
class LoginFragment : BaseFragment() {

    override val kodeinBuilder: (Kodein.MainBuilder.() -> Unit)? = {
        importOnce(loginModule)
    }

    private val loginViewModelFactory: LoginViewModelFactory by instance(arg = "login")
    private val loginViewModel by viewModels<LoginViewModel> { loginViewModelFactory }
    lateinit var binding: FragmentLoginBinding

    @Suppress("DEPRECATION")
    private val progressDialog by lazy {
        ProgressDialog(requireContext()).apply {
            this.isIndeterminate = true
            this.setCanceledOnTouchOutside(false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = bind()
        binding.viewModel = loginViewModel

        binding.setRestoreClickListener {
            requireActivity().supportFragmentManager.commit {
                addToBackStack(null)
                replace(R.id.content, newRestoreFragment(loginViewModel.login.value.orEmpty()))
            }
        }

        loginViewModel.networkErrorEvent.observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let {
                view.snackbar(it, getString(R.string.retry)) {
                    loginViewModel.auth()
                }
            }
        })

        val onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) loginViewModel.clearErrors()
        }
        binding.loginEditText.onFocusChangeListener = onFocusChangeListener
        binding.passwordEditText.onFocusChangeListener = onFocusChangeListener

        loginViewModel.successLoginEvent.observe(this, Observer {
            requireFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            requireFragmentManager().commit {
                replace(R.id.content, MainFragment())
            }
        })

        loginViewModel.networkState.observe(this, Observer {
            if (it.status == Status.RUNNING) {
                progressDialog.show()
            } else {
                progressDialog.dismiss()
            }
        })
    }

    override fun onDestroyView() {
        binding.unbind()
        super.onDestroyView()
    }
}
