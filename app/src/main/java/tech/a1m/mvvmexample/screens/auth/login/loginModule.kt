package tech.a1m.mvvmexample.screens.auth.login

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.factory
import org.kodein.di.generic.instance

val loginModule = Kodein.Module(name = "loginModule") {
    bind<LoginViewModelFactory>() with factory { login: String ->
        LoginViewModelFactory(login, instance(), instance(), instance())
    }
}