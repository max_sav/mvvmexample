package tech.a1m.mvvmexample.screens.auth.restore

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import tech.a1m.mvvmexample.Mockable
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.modules.api.Api
import tech.a1m.mvvmexample.utills.Event
import tech.a1m.mvvmexample.utills.NetworkState
import tech.a1m.mvvmexample.utills.Status
import java.util.concurrent.TimeUnit

@Mockable
class RestoreViewModel(
    private val _login: String,
    private val api: Api,
    application: Application
) : AndroidViewModel(application) {
    private val disposables = CompositeDisposable()

    val login = MutableLiveData<String>().apply { value = _login }

    private val internalNetworkState = MutableLiveData<NetworkState>().apply {
        value = NetworkState.LOADED
    }
    val networkState: LiveData<NetworkState> get() = internalNetworkState

    val networkErrorEvent: LiveData<Event<String?>> = internalNetworkState.map {
        Event(
            if (it.status == Status.FAILED) {
                it.msg
            } else {
                null
            }
        )
    }

    private val internalLoginError = MutableLiveData<String?>()
    val loginError: LiveData<String?> get() = internalLoginError

    private val internalSuccessLoginEvent = MutableLiveData<Event<Unit>>()
    val successLoginEvent: LiveData<Event<Unit>> get() = internalSuccessLoginEvent

    private val internalRestoredPassword = MutableLiveData<String?>()
    val restoredPassword: LiveData<String?> get() = internalRestoredPassword

    private val loginObserver: (String) -> Unit = { internalRestoredPassword.value = null }

    init {
        login.observeForever(loginObserver)
    }

    fun restorePassword() {
        val login = this.login.value ?: return
        Single.fromCallable { login }
            .subscribeOn(Schedulers.io())
            .delay(5, TimeUnit.SECONDS)
            .flatMap { api.restorePassword(login) }
            .doOnSubscribe { internalNetworkState.postValue(NetworkState.LOADING) }
            .observeOn(Schedulers.computation())
            .subscribe({
                internalNetworkState.postValue(NetworkState.LOADED)
                if (!it.isNullOrEmpty()) {
                    internalRestoredPassword.postValue(it)
                    clearErrors()
                } else {
                    internalLoginError.postValue(
                        getApplication<Application>().getString(R.string.user_not_found)
                    )
                }
            }, {
                if (it is NullPointerException) {
                    internalNetworkState.postValue(NetworkState.LOADED)
                    internalLoginError.postValue(
                        getApplication<Application>().getString(R.string.user_not_found)
                    )
                } else {
                    internalNetworkState.postValue(
                        NetworkState.error(
                            getApplication<Application>().getString(R.string.network_error)
                        )
                    )
                }
            })
            .addTo(disposables)
    }

    fun clearErrors() {
        internalLoginError.postValue(null)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public override fun onCleared() {
        disposables.dispose()
        login.removeObserver(loginObserver)
        super.onCleared()
    }
}

@Mockable
class RestoreViewModelFactory(
    private val login: String,
    private val api: Api,
    private val application: Application
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) = RestoreViewModel(
        login,
        api,
        application
    ) as T
}