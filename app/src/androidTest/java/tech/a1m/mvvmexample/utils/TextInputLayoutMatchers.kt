package tech.a1m.mvvmexample.utils

import android.view.View
import com.google.android.material.textfield.TextInputLayout
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

object TextInputLayoutMatchers {
    fun withErrorEnabled(): Matcher<View> {
        return object : TypeSafeMatcher<View>(TextInputLayout::class.java) {

            override fun describeTo(description: Description) {
                description.appendText("TextInputLayout error enabled")
            }

            override fun matchesSafely(view: View): Boolean {
                val item = view as TextInputLayout
                return item.isErrorEnabled
            }
        }
    }

    fun withErrorNotEnabled(): Matcher<View> {
        return object : TypeSafeMatcher<View>(TextInputLayout::class.java) {

            override fun describeTo(description: Description) {
                description.appendText("TextInputLayout error not enabled")
            }

            override fun matchesSafely(view: View): Boolean {
                val item = view as TextInputLayout
                return !item.isErrorEnabled
            }
        }
    }

    fun withErrorText(text: CharSequence?): Matcher<View> {
        return object : TypeSafeMatcher<View>(TextInputLayout::class.java) {

            override fun describeTo(description: Description) {
                description.appendText("TextInputLayout error text")
            }

            override fun matchesSafely(view: View): Boolean {
                val item = view as TextInputLayout
                return item.error == text
            }
        }
    }
}