package tech.a1m.mvvmexample.screens.auth.login

import android.app.Application
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.factory
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.databinding.FragmentLoginBinding
import tech.a1m.mvvmexample.extentions.bind
import tech.a1m.mvvmexample.modules.api.Api
import tech.a1m.mvvmexample.modules.settings.AuthInfo
import tech.a1m.mvvmexample.utils.TextInputLayoutMatchers.withErrorText

@RunWith(AndroidJUnit4::class)
class LoginFragmentTest {

    lateinit var scenario: FragmentScenario<LoginFragment>
    lateinit var viewModel: LoginViewModel
    lateinit var api: Api

    @Before
    fun setUp() {
        api = mock()

        scenario = launchFragmentInContainer(themeResId = R.style.AppTheme) {
            LoginFragment().apply {
                testKodeinModule = Kodein.Module("testModule") {
                    bind<Api>(overrides = true) with singleton { api }
                    bind<LoginViewModelFactory>(overrides = true) with factory { login: String ->
                        TestedLoginViewModelFactory(login, instance(), instance(), instance())
                    }
                }
            }
        }

    }

    @Test
    fun recreateTest() {
        scenario.recreate()
    }

    @Test
    fun authClickTest() {
        whenever(api.auth(any(), any()))
            .thenReturn(Single.never())

        onView(withId(R.id.loginEditText)).perform(clearText())
        onView(withId(R.id.loginEditText)).perform(typeText("login"))

        onView(withId(R.id.passwordEditText)).perform(clearText())
        onView(withId(R.id.passwordEditText)).perform(typeText("password"))

        assertEquals(viewModel.login.value, "login")
        assertEquals(viewModel.password.value, "password")

        onView(withId(R.id.loginButton)).perform(click())
        verify(viewModel).auth(eq("login"), eq("password"))
    }

    @Test
    fun passwordErrorTest() {
        val errorText = "password error"

        scenario.onFragment {
            (viewModel.passwordError as MutableLiveData).value = errorText
            it.bind<FragmentLoginBinding>().executePendingBindings()
        }
        onView(withText(errorText)).check(matches(isDisplayed()))
        onView(withId(R.id.passwordInputLayout)).check(matches(withErrorText(errorText)))

        onView(withId(R.id.loginEditText)).perform(click())
        onView(withId(R.id.passwordEditText)).perform(click())

        scenario.onFragment {
            it.bind<FragmentLoginBinding>().executePendingBindings()
        }
        onView(withId(R.id.passwordInputLayout)).check(matches(withErrorText(null)))
    }

    @Test
    fun loginAndPasswordChangesTest() {
        scenario.onFragment {
            viewModel.login.value = "newLogin"
            viewModel.password.value = "newPassword"
            it.bind<FragmentLoginBinding>().executePendingBindings()
        }
        onView(withText("newLogin")).check(matches(isDisplayed()))
        onView(withText("newPassword")).check(matches(isDisplayed()))
    }

    private inner class TestedLoginViewModelFactory(
        login: String,
        api: Api,
        authInfo: AuthInfo,
        application: Application
    ) : LoginViewModelFactory(login, api, authInfo, application) {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            val viewModel = super.create(modelClass)
            this@LoginFragmentTest.viewModel = spy(viewModel as LoginViewModel)
            @Suppress("UNCHECKED_CAST")
            return this@LoginFragmentTest.viewModel as T
        }
    }
}