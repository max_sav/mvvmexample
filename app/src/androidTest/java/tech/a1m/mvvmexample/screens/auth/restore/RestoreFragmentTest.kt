package tech.a1m.mvvmexample.screens.auth.restore

import android.app.Application
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.factory
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.databinding.FragmentRestorePasswordBinding
import tech.a1m.mvvmexample.extentions.bind
import tech.a1m.mvvmexample.modules.api.Api
import tech.a1m.mvvmexample.utils.TextInputLayoutMatchers.withErrorText

class RestoreFragmentTest {

    lateinit var scenario: FragmentScenario<RestoreFragment>
    lateinit var viewModel: RestoreViewModel
    lateinit var api: Api

    @Before
    fun setUp() {
        api = mock()

        scenario = launchFragmentInContainer(
            themeResId = R.style.AppTheme,
            fragmentArgs = newRestoreFragmentBundle("login")
        ) {
            RestoreFragment().apply {
                arguments = it
                testKodeinModule = Kodein.Module("testModule") {
                    bind<Api>(overrides = true) with singleton { api }
                    bind<RestoreViewModelFactory>(overrides = true) with factory { login: String ->
                        TestedRestoreViewModelFactory(login, instance(), instance())
                    }
                }
            }
        }

    }

    @Test
    fun recreateTest() {
        scenario.recreate()
        scenario.onFragment {
            assertEquals(it.login, "login")
        }
    }

    @Test
    fun restoreClickTest() {
        whenever(api.restorePassword(any()))
            .thenReturn(Single.never())

        onView(withId(R.id.loginEditText)).perform(clearText())
        onView(withId(R.id.loginEditText)).perform(typeText("login"))

        assertEquals(viewModel.login.value, "login")

        onView(withId(R.id.restoreButton)).perform(click())
        verify(viewModel).restorePassword()
    }

    @Test
    fun loginErrorTest() {
        val errorText = "login error"

        scenario.onFragment {
            (viewModel.loginError as MutableLiveData).value = errorText
            it.bind<FragmentRestorePasswordBinding>().executePendingBindings()
        }
        onView(withText(errorText)).check(matches(isDisplayed()))
        onView(withId(R.id.loginInputLayout)).check(matches(withErrorText(errorText)))

        onView(withId(R.id.loginEditText)).perform(typeText("newLogin"))

        scenario.onFragment {
            it.bind<FragmentRestorePasswordBinding>().executePendingBindings()
        }
        onView(withId(R.id.loginInputLayout)).check(matches(withErrorText(null)))
    }

    @Test
    fun loginChangesTest() {
        scenario.onFragment {
            viewModel.login.value = "newLogin"
            it.bind<FragmentRestorePasswordBinding>().executePendingBindings()
        }
        onView(withText("newLogin")).check(matches(isDisplayed()))
    }

    private inner class TestedRestoreViewModelFactory(
        login: String,
        api: Api,
        application: Application
    ) : RestoreViewModelFactory(login, api, application) {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            val viewModel = super.create(modelClass)
            this@RestoreFragmentTest.viewModel = spy(viewModel as RestoreViewModel)
            @Suppress("UNCHECKED_CAST")
            return this@RestoreFragmentTest.viewModel as T
        }
    }
}