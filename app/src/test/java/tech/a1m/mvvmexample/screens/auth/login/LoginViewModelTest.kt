package tech.a1m.mvvmexample.screens.auth.login

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.RxImmediateSchedulerRule
import tech.a1m.mvvmexample.modules.api.Api
import tech.a1m.mvvmexample.modules.settings.AuthInfo
import tech.a1m.mvvmexample.utills.Event
import tech.a1m.mvvmexample.utills.NetworkState
import java.io.IOException

const val LOGIN = "login"

class LoginViewModelTest {

    @JvmField
    @Rule
    val liveDataRule: TestRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    var rxjavaRule = RxImmediateSchedulerRule()

    private lateinit var api: Api
    private lateinit var authInfo: AuthInfo
    private lateinit var application: Application
    private lateinit var loginViewModelFactory: LoginViewModelFactory
    private lateinit var loginViewModel: LoginViewModel

    @Before
    fun setUp() {
        api = mock()
        authInfo = mock()
        application = mock()
        loginViewModelFactory = LoginViewModelFactory(LOGIN, api, authInfo, application)
        loginViewModel = loginViewModelFactory.create(LoginViewModel::class.java)

        whenever(application.getString(R.string.bad_password)).thenReturn("bad_password")
        whenever(application.getString(R.string.network_error)).thenReturn("network_error")
    }

    @Test
    fun failNetworkAuth() {
        whenever(api.auth(any(), any()))
            .thenReturn(Single.error(IOException()))

        val networkTestObserver = loginViewModel.networkState.test()
        val networkErrorEventTestObserver = loginViewModel.networkErrorEvent.test()

        loginViewModel.auth()

        Assert.assertEquals(
            networkTestObserver.valueHistory(),
            listOf(
                NetworkState.LOADED,
                NetworkState.LOADING,
                NetworkState.error("network_error")
            )
        )

        networkErrorEventTestObserver.assertValue(Event("network_error"))

        loginViewModel.networkState.removeObserver(networkTestObserver)
        loginViewModel.networkErrorEvent.removeObserver(networkErrorEventTestObserver)
    }

    /**
     * now same as @see failNetworkAuth()
     */
    @Test
    fun undefinedException() {
        whenever(api.auth(any(), any()))
            .thenReturn(Single.error(Exception()))


        val networkTestObserver = loginViewModel.networkState.test()
        val networkErrorEventTestObserver = loginViewModel.networkErrorEvent.test()

        loginViewModel.auth()

        Assert.assertEquals(
            networkTestObserver.valueHistory(),
            listOf(
                NetworkState.LOADED,
                NetworkState.LOADING,
                NetworkState.error("network_error")
            )
        )
        networkErrorEventTestObserver.assertValue(Event("network_error"))

        loginViewModel.networkState.removeObserver(networkTestObserver)
        loginViewModel.networkErrorEvent.removeObserver(networkErrorEventTestObserver)
    }

    @Test
    fun errorLogin() {
        whenever(api.auth(any(), any()))
            .thenReturn(Single.just(false))

        val networkTestObserver = loginViewModel.networkState.test()
        val passwordTestObserver = loginViewModel.passwordError.test()

        loginViewModel.networkErrorEvent.test().value()

        loginViewModel.auth()

        Assert.assertEquals(
            networkTestObserver.valueHistory(),
            listOf(
                NetworkState.LOADED,
                NetworkState.LOADING,
                NetworkState.LOADED
            )
        )

        passwordTestObserver.assertValue("bad_password")

        loginViewModel.networkState.removeObserver(networkTestObserver)
        loginViewModel.passwordError.removeObserver(passwordTestObserver)
    }

    @Test
    fun errorPasswordAndClearError() {
        whenever(api.auth(any(), any()))
            .thenReturn(Single.just(false))

        val networkTestObserver = loginViewModel.networkState.test()
        val passwordTestObserver = loginViewModel.passwordError.test()

        loginViewModel.auth()

        Assert.assertEquals(
            networkTestObserver.valueHistory(),
            listOf(
                NetworkState.LOADED,
                NetworkState.LOADING,
                NetworkState.LOADED
            )
        )

        passwordTestObserver.assertValue("bad_password")

        loginViewModel.clearErrors()
        passwordTestObserver.assertValue(null as String?)

        loginViewModel.networkState.removeObserver(networkTestObserver)
        loginViewModel.passwordError.removeObserver(passwordTestObserver)
    }

    /**
     * now same as @see errorLogin()
     */
    @Test
    fun successAuth() {
        whenever(api.auth(any(), any()))
            .thenReturn(Single.just(true))

        val networkTestObserver = loginViewModel.networkState.test()
        val passwordTestObserver = loginViewModel.passwordError.test()
        val successLoginEventTestObserver = loginViewModel.successLoginEvent.test()

        loginViewModel.auth()

        Assert.assertEquals(
            networkTestObserver.valueHistory(),
            listOf(
                NetworkState.LOADED,
                NetworkState.LOADING,
                NetworkState.LOADED
            )
        )

        loginViewModel.networkState.removeObserver(networkTestObserver)
        passwordTestObserver.assertNoValue()
        successLoginEventTestObserver
            .assertValue(Event(Unit))

        loginViewModel.networkState.removeObserver(networkTestObserver)
        loginViewModel.passwordError.removeObserver(passwordTestObserver)
        loginViewModel.successLoginEvent.removeObserver(successLoginEventTestObserver)
    }

    @After
    fun tearDown() {
        loginViewModel.onCleared()
    }
}