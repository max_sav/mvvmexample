package tech.a1m.mvvmexample.screens.auth.restore

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import tech.a1m.mvvmexample.R
import tech.a1m.mvvmexample.RxImmediateSchedulerRule
import tech.a1m.mvvmexample.modules.api.Api
import tech.a1m.mvvmexample.modules.settings.AuthInfo
import tech.a1m.mvvmexample.utills.Event
import tech.a1m.mvvmexample.utills.NetworkState

const val LOGIN = "login"

class RestoreViewModelTest {

    @JvmField
    @Rule
    val liveDataRule: TestRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    var rxjavaRule = RxImmediateSchedulerRule()

    private lateinit var api: Api
    private lateinit var authInfo: AuthInfo
    private lateinit var application: Application
    private lateinit var restoreViewModel: RestoreViewModel
    private lateinit var restoreViewModelFactory: RestoreViewModelFactory

    @Before
    fun setUp() {
        api = mock()
        authInfo = mock()
        application = mock()
        restoreViewModelFactory = RestoreViewModelFactory(LOGIN, api, application)
        restoreViewModel = restoreViewModelFactory.create(RestoreViewModel::class.java)

        whenever(application.getString(R.string.network_error)).thenReturn("network_error")
        whenever(application.getString(R.string.user_not_found)).thenReturn("user_not_found")
    }

    // RESTORE PASSWORD
    @Test
    fun failRestorePassword() {
        whenever(api.restorePassword(any()))
            .thenReturn(Single.error(Exception()))

        val networkTestObserver = restoreViewModel.networkState.test()
        val networkErrorEventTestObserver = restoreViewModel.networkErrorEvent.test()

        restoreViewModel.restorePassword()

        Assert.assertEquals(
            networkTestObserver.valueHistory(),
            listOf(
                NetworkState.LOADED,
                NetworkState.LOADING,
                NetworkState.error("network_error")
            )
        )

        networkErrorEventTestObserver.assertValue(Event("network_error"))

        restoreViewModel.networkState.removeObserver(networkTestObserver)
        restoreViewModel.networkErrorEvent.removeObserver(networkErrorEventTestObserver)
    }

    @Test
    fun errorRestorePasswordAndClearErrors() {
        whenever(api.restorePassword(any()))
            .thenReturn(Single.fromCallable { null })

        val networkTestObserver = restoreViewModel.networkState.test()
        val restoredPasswordTestObserver = restoreViewModel.restoredPassword.test()
        val loginErrorTestObserver = restoreViewModel.loginError.test()

        restoreViewModel.restorePassword()

        Assert.assertEquals(
            networkTestObserver.valueHistory(),
            listOf(
                NetworkState.LOADED,
                NetworkState.LOADING,
                NetworkState.LOADED
            )
        )

        restoredPasswordTestObserver.assertValue(null as String?)

        loginErrorTestObserver.assertValue("user_not_found")

        restoreViewModel.clearErrors()
        loginErrorTestObserver.assertValue(null as String?)

        restoreViewModel.networkState.removeObserver(networkTestObserver)
        restoreViewModel.restoredPassword.removeObserver(restoredPasswordTestObserver)
        restoreViewModel.loginError.removeObserver(loginErrorTestObserver)
    }

    @Test
    fun sucessRestorePassword() {
        whenever(api.restorePassword(any()))
            .thenReturn(Single.fromCallable { "1234" })

        val networkTestObserver = restoreViewModel.networkState.test()
        val restoredPasswordTestObserver = restoreViewModel.restoredPassword.test()
        val loginErrorTestObserver = restoreViewModel.loginError.test()

        restoreViewModel.restorePassword()

        Assert.assertEquals(
            networkTestObserver.valueHistory(),
            listOf(
                NetworkState.LOADED,
                NetworkState.LOADING,
                NetworkState.LOADED
            )
        )

        restoredPasswordTestObserver.assertValue("1234")
        loginErrorTestObserver.assertValue(null as String?)

        restoreViewModel.networkState.removeObserver(networkTestObserver)
        restoreViewModel.restoredPassword.removeObserver(restoredPasswordTestObserver)
        restoreViewModel.loginError.removeObserver(loginErrorTestObserver)
    }

    @After
    fun tearDown() {
        restoreViewModel.onCleared()
    }
}